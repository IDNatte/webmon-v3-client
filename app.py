from prober.telemetry import SystemTelemetryProber
from uploader.publisher import TelemetryPublisherThread

# import multiprocessing
import threading
import datetime
import requests
import signal
import shutil
import socket
import queue
import toml
import sys
import os

# debug
import time

import logging
from logging.handlers import RotatingFileHandler


class WebmonAgent:
    def __init__(self):
        self.base_path = os.path.dirname(os.path.abspath(__file__))
        self.config = os.path.join(self.base_path, "config")
        self.config_file = os.path.join(self.config, "config.toml")

        try:
            with open(self.config_file, "r") as config:
                config_data = toml.load(config)
                telemetry_config = config_data.get("telemetry")
                logger_config = os.path.abspath(
                    config_data.get("app").get("app_log_path")
                )

                # Logger setup
                if not os.path.isdir(logger_config):
                    os.mkdir(logger_config)

                self.logger = logging.getLogger(__file__)
                self.file_logger = RotatingFileHandler(
                    os.path.join(logger_config, "app_logger.log"),
                    maxBytes=5 * 1024 * 1024,
                )

                formatter = logging.Formatter(
                    "[%(name)s] [%(asctime)s] %(levelname)s @ %(module)s : %(message)s"
                )

                self.file_logger.setFormatter(formatter)
                self.file_logger.setLevel(logging.DEBUG)
                self.logger.setLevel(logging.DEBUG)
                self.logger.addHandler(self.file_logger)

                self.telemetry_config = telemetry_config

                # telemetry setup
                self.telemetry = TelemetryPublisherThread(
                    broker=telemetry_config.get("broker"),
                    port=telemetry_config.get("port"),
                    username=telemetry_config.get("username"),
                    password=telemetry_config.get("password"),
                    client_id=telemetry_config.get("cl_id"),
                    topic=telemetry_config.get("topic"),
                    logger=self.logger,
                )

                # prober setup
                self.prober_payload = queue.Queue()
                self.prober = SystemTelemetryProber(self.prober_payload, self.logger)

                self.prober.daemon = True
                self.telemetry.daemon = True

        except FileNotFoundError as _:
            print("\n[!] Please run config_maker first...!!\n")
            sys.exit(1)

    def __get_serverkey(self):
        server_filepath = os.path.join("./pubkey")
        try:
            os.mkdir(server_filepath)

        except FileExistsError as _:
            shutil.rmtree(server_filepath)
            os.mkdir(server_filepath)

        pubkey_data = requests.get(self.telemetry_config.get("server_pubkey"))
        with open(
            os.path.join(server_filepath, "server_pubkey.pem"), "wb"
        ) as downloaded_pubkey:
            downloaded_pubkey.write(pubkey_data.content)

            self.telemetry.set_pubkey(
                os.path.join(os.path.join(server_filepath, "server_pubkey.pem"))
            )

    def __sigterm_handler(self, signal_number, frame):
        self.__stop()

    def __stop(self, status=0, err_msg=None):
        if err_msg:
            self.logger.error(err_msg)

        self.logger.warning("Stopping system")

        self.telemetry.stop()
        sys.exit(status)

    def __telemetry(self):
        self.__get_serverkey()
        self.prober.start()
        self.telemetry.run(self.prober_payload)

    def run(self):
        try:
            signal.signal(signal.SIGTERM, self.__sigterm_handler)
            self.logger.info("Running Application")
            self.__telemetry()

        except KeyboardInterrupt as _:
            self.__stop()

        except Exception as error:
            self.logger.error(error)
            self.__stop(1, error)


if __name__ == "__main__":
    app = WebmonAgent()
    app.run()
