import getpass
import secrets
import socket
import toml
import os


def config_maker_main():
    base_config_folder = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "config"
    )

    if not os.path.isdir(base_config_folder) or not os.path.isfile(
        os.path.join(base_config_folder, "config.toml")
    ):
        while True:
            try:
                app_logger_path = str(
                    input(
                        'Where do you want to put Application logger ? (E.g. "/var/log/webmon_app") : '
                    ).strip()
                    or "/var/log/webmon/webmon_client/"
                )

                tele_broker = str(
                    input('MQTT broker, E.g "mqtt.eclipse.org" : ').strip()
                    or "127.0.0.1"
                )

                tele_broker_port = int(
                    input("MQTT broker port, E.g 1234 : ").strip() or 1883
                )

                tele_username = str(
                    input(
                        'MQTT broker username, E.g "admin" (default : admin) : '
                    ).strip()
                    or "admin"
                )

                tele_password = str(
                    getpass.getpass(prompt='MQTT broker password, E.g "admin123" : ')
                    or "admin"
                )

                tele_topic = str(
                    input(
                        "Device name E.g myhostname (default : current hostname) : "
                    ).strip()
                    or socket.gethostname()
                )

                server_pubkey = str(
                    input(
                        "Server public key, ask your admin to get one (you can change it @ setting under [telemetry]) : "
                    ).strip()
                )

                print("[*]Creating folder config...")
                if not os.path.isdir(base_config_folder):
                    os.mkdir(base_config_folder)
                print("[!]Folder config created...\n")

                print("[*]Writting config to file")

                with open(
                    os.path.join(base_config_folder, "config.toml"), "w"
                ) as c_write:
                    config = {
                        "app": {"app_log_path": app_logger_path},
                        "telemetry": {
                            "broker": tele_broker,
                            "port": tele_broker_port,
                            "username": tele_username,
                            "password": tele_password,
                            "cl_id": f"webmonag-{secrets.token_urlsafe(32)}",
                            "topic": f"webmon/telemetry/{tele_topic}",
                            "server_pubkey": server_pubkey,
                        },
                    }

                    toml.dump(config, c_write)
                print("[!]Config created, running next procedure")

                break

            except KeyboardInterrupt:
                print("\nconfig creation canceled !")
                break
    else:
        print("\n[!] Config already exists, exitting...!\n")


if __name__ == "__main__":
    config_maker_main()
