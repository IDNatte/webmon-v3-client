from concurrent.futures import ThreadPoolExecutor

import subprocess
import threading
import platform
import datetime
import msgpack
import psutil
import socket
import json
import sys


class SystemTelemetryProber(threading.Thread):
    def __init__(self, result_queue, logger):
        super().__init__()
        self._result = result_queue
        self.logger = logger

    def __get_app_net(self):
        app_net_usage = {}
        for conn in psutil.net_connections(kind="inet"):
            try:
                if (
                    conn.status == psutil.CONN_ESTABLISHED
                    or conn.status == psutil.CONN_LISTEN
                    or conn.status == psutil.CONN_LAST_ACK
                ):
                    proc_name = (
                        psutil.Process(conn.pid).name()
                        if conn.pid and psutil.Process(conn.pid)
                        else "N/A"
                    )
                    if proc_name not in app_net_usage:
                        app_net_usage[proc_name] = {
                            "proc_name": proc_name,
                            "pid": conn.pid if conn.pid else "N/A",
                            "status": conn.status.capitalize(),
                            "address": {
                                "local_address": conn.laddr[0] if conn.laddr else "N/A",
                                "local_port": conn.laddr[1] if conn.laddr else "N/A",
                                "remote_address": (
                                    conn.raddr[0] if conn.raddr else "N/A"
                                ),
                                "remote_port": conn.raddr[1] if conn.raddr else "N/A",
                            },
                        }
                    else:
                        app_net_usage[proc_name]["status"] = conn.status.capitalize()
            except (psutil.NoSuchProcess, psutil.AccessDenied) as _:
                pass

        return sorted(
            app_net_usage.values(),
            key=lambda pid: (
                -float("inf") if pid.get("pid") == "N/A" else pid.get("pid")
            ),
        )

    def __get_app_list(self):
        running_app = []
        try:
            with ThreadPoolExecutor() as executor:
                processes = list(psutil.process_iter(["pid", "name", "username"]))
                cpu_percents = executor.map(
                    lambda p: psutil.cpu_percent(interval=0.0),
                    range(len(processes)),
                )
                mem_percents = executor.map(
                    lambda p: psutil.Process(p.info["pid"]).memory_percent(), processes
                )

                for proc, cpu_percent, mem_percent in zip(
                    processes, cpu_percents, mem_percents
                ):
                    running_app.append(
                        {
                            "proc_name": proc.info.get("name"),
                            "proc_info": proc.info,
                            "user": proc.info.get("username"),
                            "used_resource": {
                                "cpu": cpu_percent,
                                "mem": mem_percent,
                            },
                        }
                    )
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass

        return running_app

    def run(self):
        try:
            while True:
                network = psutil.net_io_counters(pernic=True)
                if platform.system() == "Linux":
                    journal_prober = subprocess.run(
                        ["journalctl", "-n", "20", "-o", "json"],
                        capture_output=True,
                        text=True,
                    )

                    probe_result = journal_prober.stdout.strip().split("\n")
                    journal = [json.loads(jour_prober) for jour_prober in probe_result]

                else:
                    journal = []

                packed = msgpack.packb(
                    {
                        "probe_time": f"{datetime.datetime.now()}",
                        "system_info": {
                            "net_info": [
                                {
                                    "interface": net,
                                    "receive": network.get(net).bytes_recv,
                                    "transfer": network.get(net).bytes_sent,
                                    "ip_addr": [
                                        net.address
                                        for net in psutil.net_if_addrs().get(net)
                                        if net.family == socket.AF_INET
                                    ],
                                }
                                for net in network
                            ],
                            "hostname": socket.gethostname(),
                            "cpu": {
                                "core": int(psutil.cpu_count(logical=True)),
                                "load": psutil.cpu_percent(interval=1),
                            },
                            "hw_temp": [
                                {"label": hw_name, "temperature": hw_info.current}
                                for hw_name, hw, in psutil.sensors_temperatures().items()
                                for hw_info in hw
                            ],
                            "disk": [
                                {
                                    "mount_point": disk_partition.mountpoint,
                                    "total": psutil.disk_usage(
                                        disk_partition.mountpoint
                                    ).total,
                                    "used": psutil.disk_usage(
                                        disk_partition.mountpoint
                                    ).used,
                                    "free": psutil.disk_usage(
                                        disk_partition.mountpoint
                                    ).free,
                                }
                                for disk_partition in psutil.disk_partitions()
                            ],
                            "memory": {
                                "percentage": psutil.virtual_memory().percent,
                                "used_memory": (
                                    psutil.virtual_memory().total
                                    - psutil.virtual_memory().available
                                ),
                                "free_memory": psutil.virtual_memory().available,
                                "total_memory": psutil.virtual_memory().total,
                            },
                        },
                        "application": {
                            "app_net_usage": self.__get_app_net(),
                            "running_app": self.__get_app_list(),
                        },
                        "journal_info": journal,
                    }
                )

                self._result.put(packed)
                self.logger.info("System Probed !")

        except KeyboardInterrupt as _:
            sys.exit()

        except Exception as e:
            self.logger.error(e)
            sys.exit(1)
