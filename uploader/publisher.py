from prober.telemetry import SystemTelemetryProber

from Crypto.Cipher import PKCS1_OAEP
from Crypto.Cipher import ChaCha20
from Crypto.PublicKey import RSA

from paho.mqtt import client

import threading
import hashlib
import base64
import json
import time
import sys
import os


class TelemetryPublisherThread(threading.Thread):
    def __init__(self, broker, port, username, password, client_id, topic, logger):
        super().__init__()
        self.broker = broker
        self.port = port
        self.username = username
        self.password = password
        self.client_id = client_id
        self.topic = topic
        self.logger = logger
        self.pubkey = None

        self.__result = None

        try:
            self.mqtt = client.Client(client_id=self.client_id)

            self.mqtt.username_pw_set(self.username, self.password)
            self.mqtt.connect(self.broker, self.port)

        except Exception as error:
            self.logger.error(error)
            self.stop()
            sys.exit(1)

    def set_pubkey(self, pubkey_path):
        self.pubkey = pubkey_path

    def run(self, payload):
        while True:
            self.mqtt.on_publish = self.__on_publish

            encrypt_payload = self.__encrypt_payload(payload.get())

            self.mqtt.publish(
                self.topic,
                payload=json.dumps(
                    {
                        "key": encrypt_payload.get("key_cipher"),
                        "nonce": encrypt_payload.get("nonce_cipher"),
                        "payload": encrypt_payload.get("data_cipher"),
                    }
                ),
            )

    def __encrypt_payload(self, payload):
        # init chacha20 key and nonce
        cipher_nonce = os.urandom(12)
        cipher_key = hashlib.shake_256(self.client_id.encode()).digest(32)

        # key cipher for chacha20
        with open(self.pubkey) as RSA_pubkey:
            public_key = RSA.import_key(RSA_pubkey.read())

        key_nonce_cipher = PKCS1_OAEP.new(public_key)

        # key & nonce cipher
        encoded_key = base64.b64encode(key_nonce_cipher.encrypt(cipher_key)).decode()
        encoded_nonce = base64.b64encode(
            key_nonce_cipher.encrypt(cipher_nonce)
        ).decode()

        # data cipher
        data_cipher = ChaCha20.new(key=cipher_key, nonce=cipher_nonce)

        encoded_data = base64.b64encode(data_cipher.encrypt(payload)).decode()

        return dict(
            {
                "key_cipher": encoded_key,
                "nonce_cipher": encoded_nonce,
                "data_cipher": encoded_data,
            }
        )

    def stop(self):
        self.mqtt.disconnect()

    def __on_publish(self, client, userdata, mid):
        self.__result = {"published": True}
        self.logger.info(f"data send to broker !")
